# BYLAWS OF GRIFFIN TECHNOLOGY ACADEMIES

A California nonprofit public benefit corporation

## Article 1 -- Offices

### Section 1. Principal Office

The principal office of the corporation for the transaction
of its business is located at 2 Positive Place, Vallejo,
CA, 94589 in Solano County, California.

### Section 2. Change of Address

The county of the corporation’s principal office can be changed only
by an amendment of these Bylaws. The Board of Directors may, however,
change the principal office from one location to another within the
named county by noting the changed address and effective date below,
and such change of address shall not be deemed an amendment of
these Bylaws:

| Principal Office Address | Effective Date |
|--------------------------|----------------|
|.                         |.               |
|.                         |.               |
|.                         |.               |

### Section 3. Other Offices

The corporation may also have other offices at such other places,
within or without the State of California, where it is qualified to
do business, as its business may require and as the Board of
Directors may, from time to time, designate.

## Article 2 -- Purposes

### Section 1. Objectives and Purposes

The purpose of the Corporation is to manage, operate, guide, direct
and promote one or more California public charter schools. Also in the
context of these purposes, the Corporation shall not, except to an
insubstantial degree, engage in any other activities or exercise of
power that do not further the purposes of the Corporation.

### Section 2. Limitations

The Corporation shall not carry on any other activities not permitted
to be carried on by: (a) a corporation exempt from federal income tax
under section 501(c)(3) of the Internal Revenue Code, or the
corresponding section of any future federal tax code; or (b) a
corporation, contributions to which are deductible under
section 170(c)(2) of the Internal Revenue Code, or the corresponding
section of any future federal tax code. No substantial part of the
activities of the Corporation shall consist of the carrying on of
propaganda, or otherwise attempting to influence legislation, and
the Corporation shall not participate in, or intervene in (including
the publishing or distributing of statements) any political campaign
on behalf of or in opposition to any candidate for public office.

## Article 3 -- Directors

### Section 1. Number of Directors

This corporation shall have at least five (5) and no more than
eleven (11) Directors, and collectively they shall be known as the
Board of Directors. In accordance with the Griffin Technology
Academies Conflict of Interest Code, employees shall not serve as
members of the Board of Directors. The number of Directors may be
changed by amendment or repeal of this Bylaw, or by adoption of a new
Bylaw. Members of the community, parents, and others may qualify as a
Director if they have demonstrated a significant commitment and
leadership in helping the corporation achieve its objectives and
purposes.

### Section 2. Powers of Directors

Subject to the provisions of the California Nonprofit Public Benefit
Corporation Law, the activities and affairs of this corporation shall
be conducted and all corporate powers shall be exercised by or under
the direction of the Board of Directors. Directors are ultimately
responsible for all corporate decisions.

### Section 3. Duties of Directors

It shall be the duty of the Directors to:

1. Perform all duties imposed on them individually and collectively
by law, by the Articles of Incorporation, or by the Bylaws of this
corporation.
2. Appoint, evaluate and discharge, the Corporate Officers, except
as otherwise provided in these Bylaws, prescribe the duties and fix
the compensation of Corporate Officers and confidential employees.
3. Supervise the Corporate Officers to assure that their duties are
performed properly.
4. Approval of Collective Bargaining Agreements for staff who are
not Corporate Officers or Confidential employees
5. Meet at such times and places as required by these Bylaws.
6. Register their physical and email addresses with the Secretary
of the corporation. Notices of meetings sent to them at such addresses
shall be valid notices.
7. Borrow money and incur indebtedness on behalf of the corporation
and cause to be executed and delivered in the name and for the
corporation’s purposes, promissory notes, bonds, debentures, deeds
of trust, mortgages, pledges, hypothecation, and other evidences of
indebtedness and securities.
8. Remove any Director, except for the representative appointed by
a charter authorizer, or officer with or without cause by a majority
of votes cast by the Board at a meeting at which a quorum is
present.

### Section 4. Designation and Term of Office of Directors

Unless otherwise removed from office in accordance with these bylaws,
each director shall hold office for three (3) years or until a successor
director has been designated and qualified. Board terms end in November.
Directors may serve unlimited consecutive terms. All directors shall
have full voting rights during open sessions. If the charter authorizer
appoints a representative to serve on the Board of Directors, the
Board of Directors may appoint an additional director to ensure an odd
number of Board members. Neither charter authorizer appointees nor
student members attend closed session meetings. All directors, except
for the representative appointed by the charter authorizer, if any,
shall be appointed by the existing Board of Directors.

### Section 5. Compensation of Directors

Directors shall serve as unpaid volunteers and may not be compensated
for services rendered to the corporation as Directors. However, they
shall be allowed reasonable reimbursement of expenses incurred in the
performance of their duties as Directors as the Board of Directors
may establish by resolution to be just and reasonable as to the
Corporation at the time that the resolution is adopted.

### Section 6. Restriction Regarding "Interested" Directors

Notwithstanding any other provision of these Bylaws, there shall be
no interested persons serving on the Board. For purposes of this Section,
"interested persons," means either:

1. Any person currently being compensated by the corporation for
services rendered it within the previous twelve (12) months, whether
as a full-time or part-time employee, independent contractor, or
otherwise, excluding reasonable compensation paid to a Director as
Director; or
2. Any brother, sister, ancestor, descendant, spouse, brother-in-law,
sister-in-law, son-in-law, daughter-in-law, mother-in-law, or
father-in-law of any suchperson; or
3. Any person over the age of twenty-one (21) at the time they are
seated as a Director.

### Section 7. Board Leadership

The Board shall elect a Chairperson and Vice Chairperson of the
Board of Directors. The Chairperson shall preside at the Board of
Directors' meetings and shall exercise and perform such other powers
and duties as the Board of Directors may assign from time to time.
In the absence of the Chairperson, the Vice Chair shall preside at
Board of Directors meetings and shall exercise and perform such other
powers and duties as the Board of Directors may assign from time to
time.

The Board of Directors may create any number of committees as
required to fulfill the duties of the Directors. All Board Committees
except the Steering Committee shall consist of two Directors appointed
by the Board of Directors and one staff person designated by the
Superintendent. With the exception of the Steering Committee, no other
committees have the authority of the Board and shall act only in an
advisory capacity.

The Board Chairperson shall appoint one less than a quorum of Board
members to serve on a Board Steering Committee, the purpose of which
is coordination of the policy issues through the establishment of the
Board meeting agendas and to carry out such other tasks
specifically assigned by full Board of Directors.

The Board Steering Committee shall have the following
responsibilities:

1. To act on behalf of the Board of Directors in the interim when
urgent attention and action is necessary.
2. To assess the performance of the Superintendent subject to Board
approval.
3. To recommend and nominate new Board members and officers whenever
a need arises.

Duties of the Chairperson:

It shall be the duty and responsibility of the Chairperson of the
corporation to:

1. Preside at all meetings.
2. Direct the Superintendent or designee in scheduling meetings of
the board of directors and in recording the proceedings of such
meetings.
3. Provide general direction of all business for the effective and
sound operation and growth of the organization, and for advising
and making recommendations to the Board of Directors.
4. Execute all decisions, contracts or other authorized instruments
of the board of directors and its duly constituted committees,
except when execution is otherwise specifically assigned by the
Bylaws.
5. Perform all duties incident to the president’s office and such
other duties as the law, the Bylaws and the board of directors
may require.

### Section 8. Meetings and Action of Committees

Meetings and actions of committees shall be governed by, noticed held and taken
in accordance with the provisions of these Bylaws concerning meetings of the
Board of Directors, with such changes in the context of such Bylaw provisions
as are necessary to substitute the committee and its members for the Board of
Directors and its members. The time for regular or special meetings of the
committees may be fixed by resolution of the Board or by the committee. Minutes
shall be kept of each meeting of any committee and shall be filed with the
corporate records. The Board may adopt rules for the governance of
any committee not inconsistent with the provisions of these Bylaws.

### Section 9. Vacancy, Resignation, Removal and Absence

1. **Vacancy.** A vacancy in the Board of Directors shall exist:
    1. On the death, resignation or removal of any Director, and
    2. Whenever the number of Directors is increased.
2. **Resignation.** A Director may resign at any time by giving written
notice to any officer of the corporation or the full Board. The resignation
shall be effective when the notice is given unless the notice specifies a
later time for the resignation to become effective. No Director may resign if
the corporation would then be left without a Director in charge of its affairs,
except upon notice to the Attorney General.
3. **Removal.** Any Director may be removed, with or without cause, by the vote
of the majority of the members of the entire Board of Directors at a special
meeting called for that purpose, or at a regular meeting, provided that notice of
that meeting and of the removal questions are given in compliance with the
provisions of the Brown Act. The representative appointed by the charter authorizer
may be removed without cause by the charter authorizer or with the written consent
of the charter authorizer. Any vacancy caused by the removal of a Director shall
be filled as provided in Section 5.
4. **Absence.** A Director is expected to attend all meetings of the Board
during the Director’s term of office and to communicate to the Board Chairperson
any absence at least forty-eight (48) hours in advance or in case of an emergency,
within twelve (12) to twenty-four (24) hours before any scheduled meeting. If for
any reason a Director or officer is unable to carry out his duties as best he can,
he agrees to discuss with the Chairperson of the Board his future obligations in
serving on the Board of Directors.
5. **Vacancies.** Vacancies on the Board of Directors may be filled by approval
of the Board of Directors or, if the number of Directors then in office is less
than a quorum, by (a) the affirmative vote of a majority of the Directors then
in office at a regular or special meeting of the Board, or (b) a sole remaining
Director. The charter authorizer may fill a vacancy in the seat held by their
representative.

### Section 10. Non-liability of Directors

The Directors shall not be personally liable for the debts, liabilities, or
other obligations of the corporation.
