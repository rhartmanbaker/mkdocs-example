# Welcome to the example policies page

This is a test site which is an example of what could be done with
school policies.

## Policy pages

- [Bylaws](bylaws.md)
- [Independent Study](independent-study.md)
