# INDEPENDENT STUDY BOARD POLICIES

These policies apply to all students participating in independent study at
Mare Island Technology Academy, MIT Academy, MIT Griffin Academy Middle
School, and Griffin Academy High School (the "Schools").

Each student’s independent study shall be coordinated, evaluated, and carried
out under the general supervision of an assigned certificated employee or
employees.

For students in all programs of independent study, the maximum length of time
that may elapse between the time an assignment is made and the date by which
the student must complete the assigned work shall be as follows:

- For students in grades six through eight, 20 days
- For students in grades nine through twelve, 30 days.

When special or extenuating circumstances justify a longer time for individual
students, the director or their designee may approve a period not to exceed an
additional 30 days.

## Missed Assignments and Level of Satisfactory Progress

When a student fails to complete >30% assignments during any period of 9
weeks or fails to make satisfactory progress (as defined below the school will
conduct an evaluation to determine whether it is in the best interests of the
student to remain in independent study or to return to the regular school
program. A written record of the findings of any evaluation made pursuant to
this subdivision shall be maintained in the student’s permanent record and
treated as a mandatory interim student record. The record shall be maintained
for a period of three years from the date of the evaluation and, if the student
transfers to another California public school, the record shall be forwarded to
that school.

Satisfactory educational progress shall be based on all of the following
indicators, as applicable:

- Student achievement and engagement, as measured by all of the following, as
applicable:
  - Statewide assessments that are part of the California Assessment of
  Student Performance and Progress (a.k.a., “CAASPP”, or any other subsequent
  assessment as certified by the state board of education),
  - The percentage of students who have successfully completed courses that
  satisfy the requirements for entrance to the University of California and
  California State University,
  - The percentage of students who have successfully completed courses that
  satisfy the requirements for career technical education sequences or
  programs that align with state board-approved career technical education
  standards and frameworks,
  - The percentage of students who have successfully completed both the
  university entrance and career technical courses specified above,
  - The percentage of English learner students who make progress toward
  English proficiency as measured by the English Language Proficiency
  Assessments for California ("ELPAC" or subsequent assessments of English
  proficiency certified by the state board),
  - The English learner reclassification rate,
  - The percentage of students who have passed an advanced placement exam
  with a score of "3" or higher, and
  - The percentage of students who demonstrate college preparedness
  pursuant to the Early Assessment Program (or any subsequent assessment of
  college preparedness).
- Student engagement, as measured by all of the following, as applicable:
  - School attendance rates,
  - Chronic absenteeism rates,
  - Middle school dropout rates,
  - High school dropout rates, and
  - High school graduation rates.
- The completion of assignments, assessments, or other indicators that
evidence that the student is working on assignments.
- Learning requirement concepts, as determined by the supervising teacher.
- Progressing toward successful completion of the course of study or
individual course, as determined by the supervising teacher,

## Academic Content

Independent study shall include the provision of content aligned to grade
level standards that is provided at a level of quality and intellectual
challenge substantially equivalent to in-person instruction.

Independent study shall include access to all courses offered by the Schools
for graduation and approved by the University of California or the California
State University as creditable under the A-G admission criteria.

## Tiered Reengagement

For all students who

- are not generating attendance for more than three schooldays or 60 percent
of the instructional days in a school week, or
- who are in violation of their written agreement, or
- who do not generate attendance for 10 percent of required instructional
time for over four continuous weeks of a school’s approved instructional
calendar, or
- who are not participatory in mandated live interaction or synchronous
instruction for more than three schooldays or for 60 percent of the scheduled
days of synchronous instruction in a school month

the school shall have procedures including the following reengagement
strategies:

- Verifying current contact information for the student,
- Notifying parents or guardians of lack of participation within one school day
of the absence or lack of participation,
- A plan for outreach from the school to determine student needs, including a
connection with health and social services, as necessary,
- A clear standard requiring a student-parent-educator conference, as defined
below, to review the student’s written agreement, reconsider the independent
study program’s impact on the student’s achievement and well-being, consistent
with the school’s policies regarding the maximum amount of time allowed between
the assignment and completion of student’s assigned work, satisfactory
educational progress, and the number of missed assignments allowed before an
evaluation of whether the student should be allowed to continue in independent
study,

For the purposes of this policy, "student-parent-educator conference" means
a meeting involving, at a minimum, all parties who signed the student's
written independent study agreement.

## Opportunities for Live Interaction and Synchronous Instruction

The Schools shall plan to provide opportunities for live interaction and
synchronous instruction as follows for all students engaged in independent
study:

- For students in grades 6 to 8 inclusive, the Schools shall plan to provide
opportunities for both daily live interaction and at least weekly synchronous
instruction for all students throughout the year,
- For students in grades 9-12 inclusive, the Schools shall plan to provide
opportunities for at least weekly synchronous instruction for all students
throughout the year,

For the purposes of this policy, "live interaction" means interaction between
the student and certificated or non-certificated staff, and may include peers,
provided for the purpose of maintaining school connectedness, including but not
limited to wellness checks, progress monitoring, provision of services, and
instruction. This live interaction may take place in-person, or in the form of
internet or telephonic communication.

For the purposes of this policy, "synchronous instruction" means
classroom-style instruction or designated small group or one-on-one instruction
delivered in person, or in the form of internet or telephonic communications,
and involving live two-way communication between the teacher

## Return to In-Person Instruction

For students whose families wish to return to in-person instruction from
independent study, the Schools shall allow the student to return expeditiously,
and in no case later than five instructional days,

## Written Agreements: (5 C.C.R. § 11702)

A current written agreement for each independent study
student shall be maintained on file for each participating student. Each
agreement shall be signed, dated, and in effect prior to the start of
reporting attendance (ADA) pursuant to that agreement. The independent study
agreement for a student will require and cover a study plan that represents
the same amount of study that would be required of a student in the classroom
and be consistent with the Schools' curriculum and course of study of
students participating in the regular classroom setting.

### Agreement Content

Each independent study written agreement shall contain at least all of the
following provisions:

- The manner, time, frequency, and place for submitting a student’s
assignments, for reporting the student’s academic progress, and for
communicating with a student’s parent or guardian regarding academic progress.
- The objectives and methods of study for the student’s work, and the methods
used to evaluate that work.
- The specific resources, including materials and personnel that will be made
available to the student. These resources shall include confirming or providing
access for all students to the connectivity and devices adequate to participate
in the academic program and complete assigned work.
- A statement of the policies adopted regarding the maximum length of time
allowed between the assignment and the completion of a student’s assigned work,
the level of satisfactory educational progress, and the number of missed
assignments allowed prior to an evaluation of whether or not the student should
be allowed to continue in independent study. The level of satisfactory
educational progress and missed assignments shall conform to the requirements
specified above in this policy.
- The duration of the independent study agreement, including the
beginning and ending dates for participating in independent study,
recognizing that no independent study agreement shall be valid for any period
longer than one school year.
- A statement of the number and title of courses or other measures of
academic accomplishment appropriate to the agreement, to be earned by the
student upon completion.
- A statement detailing the academic and other supports that will be provided
to address the needs of students who are not performing at grade level, or
need support in other areas such as English learners, individuals with
exceptional needs as required to be consistent with the student’s
individualized education program or plan pursuant to Section 504 of the
Rehabilitation Act of 1973 (29 U.S.C. Sec. 794), students in foster care,
students experiencing homelessness, and students requiring mental health
supports.
- The inclusion of a statement in each independent study agreement that
independent study is an optional educational alternative in which no student
may be required to participate.

Written agreements shall be signed, prior to the commencement of independent
study, by the student, the student’s parent/guardian/caregiver if the student
is less than 18 years of age, the certificated employee designated as
responsible for the general supervision of independent study, and all persons
who have direct responsibility for providing assistance to the student.
Written agreements may be maintained electronically along with and may include
subsidiary agreements, such as course contracts and assignment and work records.
Written agreements may be signed using electronic signatures that comply with
applicable state and federal standards and are intended by the signatory to have
the same effect as a handwritten signature. Before signing a written agreement
pursuant to this section, upon the request of the parent or guardian of a
student, the appropriate School shall conduct a phone, videoconference, or
in-person student-parent-educator conference or other school meeting during
which the student, parent or guardian, and, if requested by the student or
parent, an education advocate, may ask questions about the educational options,
including which curriculum offerings and nonacademic supports will be available
to the student in independent study, before making the decision about enrollment
or disenrollment in the various options for learning.

For the 2021-22 school year only, written agreements may be completed and
signed as provided above no later than 30 days after the first day of
instruction in an independent study program, or October 15, whichever date
comes later.

Adopted: 8/10/2021

Amended:10/12/2021
